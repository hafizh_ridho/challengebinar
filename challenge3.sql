-- DBL
CREATE DATABASE challenge3;

CREATE TABLE produk (
  "id_produk" SERIAL PRIMARY KEY,
  "nama_produk" VARCHAR(20),
  "kuantitas" INTEGER
);

CREATE TABLE pemasok (
  "id_pemasok" SERIAL PRIMARY KEY,
  "komponen_disuplai" VARCHAR(20)
);



CREATE TABLE komponen (
  "id_komponen" SERIAL PRIMARY KEY,
  "id_produk" INTEGER REFERENCES "produk" ("id_produk"),
  "id_pemasok" INTEGER REFERENCES "pemasok" ("id_pemasok"),
  "nama_komponen" VARCHAR(20),
  "deskripsi" TEXT
);

-- DML

INSERT INTO produk (nama_produk, kuantitas) VALUES ('botol minum', 10); 

INSERT INTO pemasok (komponen_disuplai) VALUES ('botol');

INSERT INTO pemasok (komponen_disuplai) VALUES ('tutup botol');

INSERT INTO komponen (nama_komponen, deskripsi) VALUES ('botol', 'berfungsi untuk menamoung air agar bisa di minum ');

INSERT INTO komponen (nama_komponen, deskripsi) VALUES ('tutup botol', 'berfungsi untuk menutup botol agar air tidak tumpah');
