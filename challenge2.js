

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

class Kelas {
  constructor() {
    this.nilai = [];
  }

  tambahNilai(n) {
    this.nilai.push(n);
  }

  nilaiTertinggi() {
    return Math.max(...this.nilai);
  }

  nilaiTerendah() {
    return Math.min(...this.nilai);
  }

  rataRata() {
    const total = this.nilai.reduce((acc, cur) => acc + cur);
    return total / this.nilai.length;
  }

  jumlahLulus() {
    const lulus = this.nilai.filter(n => n >= 60);
    return lulus.length;
  }

  jumlahGagal() {
    const tidakLulus = this.nilai.filter(n => n < 60);
    return tidakLulus.length;
  }

  urutkanNilai() {
    return this.nilai.sort((a, b) => a - b);
  }

  cariNilaiSiswa(n) {
    const siswa = this.nilai.filter(x => x == n);
    return siswa;
  }
}

const kelas = new Kelas();

rl.setPrompt('Masukkan nilai siswa (ketik "s" jika sudah selesai): ');
rl.prompt();

rl.on('line', (jawaban) => {
  if (jawaban.toLowerCase() == 's') {
    console.log(`Nilai tertinggi: ${kelas.nilaiTertinggi()}`);
    console.log(`Nilai terendah: ${kelas.nilaiTerendah()}`);
    console.log(`Jumlah siswa lulus: ${kelas.jumlahLulus()}, Jumlah siswa yang gagal: ${kelas.jumlahGagal()}`);
    console.log(`Urutan nilai siswa dari terendah ke tertinggi: ${kelas.urutkanNilai()}`);
    console.log(`Siswa dengan nilai 90 dan 100: ${kelas.cariNilaiSiswa(90)}, ${kelas.cariNilaiSiswa(100)}`);
    rl.close();
  } else {
    const nilai = parseInt(jawaban);
    kelas.tambahNilai(nilai);
    rl.prompt();
  }
});
